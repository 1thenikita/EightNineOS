import { Message } from 'discord.js';
import { CommandsText } from '../../logs/commands/text';
import { UsersEntity } from '../../database/entities/Users.entity';
import { getConfig } from '../../config/config';
import { noun } from 'plural-ru';

export const moveStats = async (
  message: Message,
  user: null,
  oldID: string,
  newID: string,
): Promise<void> => {
  if (!oldID || !newID) throw CommandsText.PARAMS_ERROR;

  const oldStats = await UsersEntity.getOrCreateUser(oldID);
  const newStats = await UsersEntity.getOrCreateUser(newID);

  newStats.warns += oldStats.warns;
  newStats.coins += oldStats.coins;
  newStats.level += oldStats.level - 1;
  newStats.voice_time += oldStats.voice_time;
  newStats.voice_time_week += oldStats.voice_time_week;
  newStats.voice_time_mod += oldStats.voice_time_mod;
  newStats.messages += oldStats.messages;

  const equip = Object.values(oldStats.equipment);
  for (let i = 0; i < equip.length; i++) {
    const oldValue = equip[i];

    newStats.equipment[i + 1] += oldValue;

    if (newStats.equipment[i + 1] > getConfig().maxEquipLevel) {
      newStats.equipment[i + 1] = getConfig().maxEquipLevel;
    }
  }

  if (newStats.warns > 2) {
    const sumWarns = noun(newStats.warns, 'варн', 'варна', 'варнов');
    message.author.send(
      `У данного человека в сумме вышло **${newStats.warns}** ${sumWarns}.\nЯ автоматически поставил ему 2 варна, дальше думайте сами...`,
    );
    newStats.warns = 2;
  }

  await newStats.save();
  await oldStats.delete();
  message.author.send(`Статистика с аккаунта \`${oldID}\` была перенесена на \`${newID}\``);
};
