import { Message } from 'discord.js';
import { CommandsText } from '../../logs/commands/text';
import { ReddcEntity } from '../../database/entities/Reddc.entity';
import { getConfig } from '../../config/config';

export const delrep = async (
  message: Message,
  user: null,
  _: string,
  stringValue: string,
): Promise<void> => {
  const target = message.mentions.members?.first();

  if (!target || !target.roles.cache.has(getConfig().roles.redactor))
    throw CommandsText.PARAMS_ERROR;

  const checkedValue = parseInt(stringValue);
  const value = isNaN(checkedValue) ? 1 : checkedValue;

  if (value < 0 || value > 5) throw 'Укажите корректное значение (больше нуля и меньше пяти)';

  const adminUser = await ReddcEntity.getOrCreateAdmin(target.id);

  const weekReports = adminUser.week_reports;

  if (value > weekReports) throw 'Кол-во снимаемых репортов превышает кол-во имеющихся';

  adminUser.week_reports -= value;
  adminUser.reports -= value;

  message.author.send(`${value} репорта(ов) было снято`);

  adminUser.save();
};
