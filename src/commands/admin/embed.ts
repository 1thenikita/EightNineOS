import { Message, MessageEmbed } from 'discord.js';

export const embed = (message: Message): void => {
  const embed = message.content.slice(7);

  try {
    const embedJSON = JSON.parse(embed);
    message.channel.send(embedJSON);
  } catch (e) {
    const embedError = new MessageEmbed()
        .setTitle("Произошла ошибка при обработке эмбеда")
        .setDescription("Проверьте наличие всех скобок, возможно Вы их упустили.")
        .addField(e.name, e.message)
        .setColor("#ff0000")
        .setTimestamp();
    return void message.author.send({ embeds: [embedError] });
  }
};
