import { Message } from 'discord.js';

export const clear = async (message: Message, user: null, stringValue: string): Promise<void> => {
  const value = parseInt(stringValue);

  if (isNaN(value) || value < 1 || value > 99)
    throw 'Укажите корректное значение (больше одного и меньше девяносто девяти)';

  const fetched = await message.channel.messages
    .fetch({ limit: value, before: message.id })
    .catch(() => null);

  if (message.channel.type !== 'DM' && fetched) {
    message.channel.bulkDelete(fetched);
  }
};
