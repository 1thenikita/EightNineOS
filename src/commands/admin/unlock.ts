import { CategoryChannel, Message, VoiceChannel } from 'discord.js';
import { sendGeneral } from '../../logs/admin/channels';
import { getConfig } from '../../config/config';
import { noun } from 'plural-ru';

export const unlock = async (message: Message): Promise<void> => {
  const channelsCache = message.guild!.channels.cache;

  const textCategory = channelsCache.get(getConfig().channels.text.general)!
    .parent as CategoryChannel;
  const moderateVoiceCategory = channelsCache.get(getConfig().channels.voice.goodCommunication)!
    .parent as CategoryChannel;
  const notModerateVoiceCategory = channelsCache.get(getConfig().channels.voice.badCommunication)!
    .parent as CategoryChannel;
  const createPrivate = channelsCache.get(getConfig().channels.voice.createRoom)! as VoiceChannel;

  const toLockVoiceCategories = [moderateVoiceCategory, notModerateVoiceCategory];

  const everyone = message.guild!.roles.everyone;
  const lockRole = getConfig().roles.isLocked;

  for (const [, voice] of textCategory!.children) {
    if (voice.permissionOverwrites.cache.find((item) => item.id === lockRole)) continue;

    await voice.permissionOverwrites.create(everyone, {
      SEND_MESSAGES: true,
    });
  }

  for (const category of toLockVoiceCategories) {
    for (const [, voice] of category!.children) {
      if (voice.permissionOverwrites.cache.find((item) => item.id === lockRole)) continue;

      await voice.permissionOverwrites.create(everyone, {
        CONNECT: true,
      });
    }
  }

  await createPrivate.permissionOverwrites.create(everyone, {
    CONNECT: true,
  });

  const sumTextChannels =
    moderateVoiceCategory!.children.size + notModerateVoiceCategory!.children.size + 1;
  const sumVoiceChannels = textCategory!.children.size;

  sendGeneral(
    `🔓 **Разблокировано**: ${sumTextChannels} текстовых ${noun(
      sumTextChannels,
      'канал',
      'канала',
      'каналов',
    )}, ${sumVoiceChannels} голосовых ${noun(sumVoiceChannels, 'канал', 'канала', 'каналов')}. 🔓`,
  );
};
