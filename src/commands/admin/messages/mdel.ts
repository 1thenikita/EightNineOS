import { Message } from 'discord.js';
import { CommandsText } from '../../../logs/commands/text';
import { getRepository } from 'typeorm';
import { MessagesEntity } from '../../../database/entities/Messages.entity';

export const mdel = async (message: Message, user: null, title: string): Promise<void> => {
  if (!title) throw CommandsText.PARAMS_ERROR;
  const pastas = await getRepository(MessagesEntity).findOne({ title });

  if (!pastas) throw CommandsText.MESSAGE_ERROR;
  await pastas.delete();

  await message.author.send('Паста успешно удалена');
};
