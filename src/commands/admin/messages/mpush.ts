import { Message } from 'discord.js';
import { CommandsText } from '../../../logs/commands/text';
import { getRepository } from 'typeorm';
import { MessagesEntity } from '../../../database/entities/Messages.entity';

export const mpush = async (
  message: Message,
  user: null,
  title: string,
  ...descriptionArr: string[]
): Promise<void> => {
  const description = descriptionArr.join(' ');

  if (!title || !description) throw CommandsText.PARAMS_ERROR;
  let name = await getRepository(MessagesEntity).findOne(title);

  if (name) {
    name.message = description;
  } else {
    name = new MessagesEntity();
    name.title = title;
    name.message = description;
  }

  name.save();
  message.author.send('Паста создана или изменена');
};
