import { Message, MessageEmbed } from 'discord.js';
import { sendGeneral, sendWarnLog } from '../../logs/admin/channels';
import { AdminTexts } from '../../logs/admin/text';
import { ReddcEntity } from '../../database/entities/Reddc.entity';
import { getConfig } from '../../config/config';
import { CommandsText } from '../../logs/commands/text';

export const ban = async (
  message: Message,
  user: null,
  memberStr: string,
  ...reasonStr: string[]
): Promise<void> => {
  const target = message.mentions.users.first();

  if (!target) throw CommandsText.PARAMS_ERROR;

  const targetMember = await message.guild!.members.fetch(target.id);
  if (!targetMember) return;
  const reason = reasonStr.join(' ');

  const embed = new MessageEmbed()
    .setColor('RED')
    .setTitle('О-паньки!?!?!')
    .setDescription(AdminTexts.BAN_USER)
    .setFooter('Причина бана: ' + reason);

  if (!target || !reason) return;
  if (getConfig().roles.admins.some((roleID) => targetMember.roles.cache.has(roleID))) return;

  sendWarnLog(`${message.author} выдал бан пользователю ${target}\nПричина: ${reason}`);
  sendGeneral(`${target} получил бан по причине: "**${reason}**"`);

  await target.send({ embeds: [embed] });
  targetMember.ban({ days: 1, reason });

  ReddcEntity.addStat(message.author, { bans: 1 });
};
