import { Message, MessageAttachment } from 'discord.js';
import { ReddcEntity } from '../../database/entities/Reddc.entity';
import { getRepository } from 'typeorm';
import { getConfig } from '../../config/config';
import AsciiTable from 'ascii-table';
import { isEmpty } from 'lodash';

export const rstats = async (message: Message): Promise<void> => {
  const adminUsers = await getRepository(ReddcEntity).find();
  if (isEmpty(adminUsers)) return;

  const table = new AsciiTable('Статистика редакторов');
  table.setHeading(
    'Редактор',
    'Репорты',
    'Репорты за неделю',
    'Муты за неделю',
    'Варны за неделю',
    'Баны за неделю',
  );

  for (const adminUser of adminUsers) {
    try {
      const member = await message.guild!.members.fetch(adminUser.user_id);
      if (!member) continue;

      if (!member.roles.cache.has(getConfig().roles.redactor)) continue;

      table.addRow(
        member.user.username,
        adminUser.reports,
        adminUser.week_reports,
        adminUser.mutes,
        adminUser.warns,
        adminUser.bans,
      );
    } catch (e) {}
  }

  const result = table.toString();
  const buffer = Buffer.from(result);
  const txtFile = new MessageAttachment(buffer, 'rstats.txt');

  await message.author.send({ files: [txtFile] });
};
