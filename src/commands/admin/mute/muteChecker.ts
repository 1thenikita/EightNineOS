import { CronJob } from 'cron';
import { getRepository } from 'typeorm';
import { PenaltiesEntity } from '../../../database/entities/Penalties.entity';
import { client } from '../../../client';
import { getConfig } from '../../../config/config';
import moment from 'moment';
import { MessageEmbed } from 'discord.js';
import { getRoleById } from '../../../utils/roles';

const checker = async () => {
  const repository = getRepository(PenaltiesEntity);
  const guild = client.guilds.cache.get(getConfig().guild);
  const currentTime = moment();
  const config = getConfig();
  const penalties = await repository.find();
  const roleMute = await getRoleById(guild!, config.roles.mute);
  const roleMuteChat = await getRoleById(guild!, config.roles.muteChat);

  if (!guild || !roleMute || !roleMuteChat) return;

  for await (const penalty of penalties) {
    const isPenaltyExpired = currentTime.isAfter(penalty.date);
    if (!isPenaltyExpired) continue;

    try {
      const member = await guild.members.fetch(penalty.user_id);
      if (member) {
        await member.roles.remove(penalty.isMuted ? roleMute : roleMuteChat);
        await member.send({
          embeds: [new MessageEmbed().setColor('BLUE').setTitle('Срок мута истёк')],
        });
      }
    } catch {}

    await repository.remove(penalty);
  }

  // bug fix for discord.js...
  for (const [id, member] of roleMute.members) {
    const isMutedInDB = await PenaltiesEntity.getPenalty(id, true);

    if (!isMutedInDB) await member.roles.remove(roleMute);
  }

  for (const [id, member] of roleMuteChat.members) {
    const isMutedInDB = await PenaltiesEntity.getPenalty(id, false);

    if (!isMutedInDB) await member.roles.remove(roleMuteChat);
  }
};

export const muteCron = new CronJob('* * * * *', checker);
