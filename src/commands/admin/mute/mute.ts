import { Message, MessageEmbed } from 'discord.js';
import { getRepository } from 'typeorm';
import { CommandsText } from '../../../logs/commands/text';
import { PenaltiesEntity } from '../../../database/entities/Penalties.entity';
import { ReddcEntity } from '../../../database/entities/Reddc.entity';
import { sendGeneral, sendMuteLog } from '../../../logs/admin/channels';
import { getConfig } from '../../../config/config';
import moment from 'moment';
import noun from 'plural-ru';
import { UsersEntity } from '../../../database/entities/Users.entity';

export const mute = async (
  message: Message,
  user: null,
  _: string,
  minutesStr: string,
  ...reasonArr: string[]
): Promise<void> => {
  const target = message.mentions.members?.first();
  const minutes = parseInt(minutesStr);
  const reason = reasonArr.join(' ');

  const repository = getRepository(PenaltiesEntity);
  const [commandName] = message.content.substring(1).split(/\s+/g);
  const isMute = commandName.toLowerCase() === 'mute';
  const roleID = isMute ? getConfig().roles.mute : getConfig().roles.muteChat;

  if (!target || !reason || isNaN(minutes)) throw CommandsText.PARAMS_ERROR;

  const isDBMuted = await PenaltiesEntity.getPenalty(target.id, isMute);
  if (isDBMuted) return void target.roles.add(roleID);

  const isRoleMuted = target.roles.cache.has(roleID);
  if (isRoleMuted) return;

  target.roles.add(roleID);

  if (target.voice.channel && isMute) target.voice.disconnect();

  const unMuteTime = moment().add(minutes, 'm');
  const penaltyEntity = repository.create({
    date: unMuteTime.toDate(),
    user_id: target.id,
    isMuted: isMute,
  });

  if (
    (reason === 'флуд' || reason === 'flud' || reason === 'flood' || reason === 'akel') &&
    target.roles.cache.has(getConfig().roles.png)
  ) {
    const targetUser = await UsersEntity.getOrCreateUser(target.id);
    targetUser.png_subscribe_timeout = null;
    target.roles.remove(getConfig().roles.png);
    target.send({
      embeds: [new MessageEmbed().setColor('RED').setTitle('Подписка на роль отменена')],
    });
  }

  await penaltyEntity.save();

  const sendTimeRu = `${minutes} ${noun(minutes, 'минуту', 'минуты', 'минут')}`;
  const embed = new MessageEmbed()
    .setColor('RED')
    .setTitle(`Вам выдан ${isMute ? 'мут' : 'мут на чат'}`)
    .addField('Причина', reason, true)
    .addField('Сроком на', sendTimeRu, true)
    .addField('Наказание выдал', `${message.author}`, true)
    .setFooter('!rreport @ник причина - обжаловать наказание');

  target.send({ embeds: [embed] });

  sendGeneral(
    `:mute: ${target} получил ${
      isMute ? 'мут' : 'мут на чат'
    } на ${sendTimeRu} по причине: "**${reason}**"`,
  );
  sendMuteLog(
    `:mute: ${message.author} выдал ${
      isMute ? 'мут' : 'мут на чат'
    } ${target} на ${sendTimeRu} по причине: "**${reason}**"`,
  );

  await ReddcEntity.addStat(message.author, { mutes: 1 });
};
