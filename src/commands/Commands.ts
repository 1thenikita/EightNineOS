import { PermissionString, Message } from 'discord.js';
import { UsersEntity } from '../database/entities/Users.entity';
import { userCommands } from './user';
import { adminCommands } from './admin';
import { clanCommands } from './clan';
import { marryCommands } from './loveRoom';

type DatabaseOptional = {
  clan: boolean;
};

export type Command<T = boolean> = {
  description: string;
  database: T;
  databaseOptional?: DatabaseOptional;
  params: string[];
  allowedRoles?: string[];
  allowedUsers?: string[];
  allowedPermissions?: PermissionString[];
  execute(message: Message, user: T extends true ? UsersEntity : null, ...params: string[]): void;
};

export type Commands<T = boolean> = {
  [name: string]: Command<T>;
};

export const Command: Commands = {
  ...userCommands,
  ...adminCommands,
  ...clanCommands,
  ...marryCommands,
};
