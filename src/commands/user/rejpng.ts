import { Message, MessageEmbed } from 'discord.js';
import { UsersEntity } from '../../database/entities/Users.entity';
import { getConfig } from '../../config/config';
import { CommandsText } from '../../logs/commands/text';

export const rejPng = async (message: Message, user: UsersEntity): Promise<void> => {
  const authorMember = await message.guild!.members.fetch(message.author.id);
  if (!authorMember) return;

  if (!authorMember.roles.cache.has(getConfig().roles.png)) throw CommandsText.DOESNT_HAVE_PNG;

  authorMember.roles.remove(getConfig().roles.png);
  user.png_subscribe_timeout = null;
  await user.save();

  authorMember.send({
    embeds: [
      new MessageEmbed().setColor('BLUE').setTitle(CommandsText.SUCCESSFULLY_REJECTED_SUBSCRIBE),
    ],
  });
};
