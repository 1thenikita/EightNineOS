import { Message } from 'discord.js';
import { sendGeneral } from '../../../logs/admin/channels';

export const vkick = async (message: Message): Promise<void> => {
  const authorMember = await message.guild!.members.fetch(message.author.id);
  const voice = authorMember.voice.channel;
  if (!voice) return;
  await message.guild!.members.fetch();
  await message.guild!.channels.fetch();
  const perm = voice.permissionOverwrites.cache
    .get(message.author.id)
    ?.allow.has('MANAGE_CHANNELS');
  if (!perm) return;

  const target = message.mentions.users.first();
  if (!target) return;

  const targetMember = await message.guild!.members.fetch(target.id);
  if (!targetMember) return;

  if (!targetMember.voice.channel) return;
  if (targetMember.voice.channel !== authorMember.voice.channel) return;
  if (target.id === message.author.id) return;

  await voice.permissionOverwrites.create(target, { CONNECT: false }).catch(() => null);

  targetMember.voice.disconnect();

  sendGeneral(`${target} был выгнан из комнаты ${message.author} 🥳`).then((msg) => {
    setTimeout(function () {
      msg.delete();
    }, 10000);
  });
};
