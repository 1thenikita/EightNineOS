import { client } from '../../../client';
import { getRepository } from 'typeorm';
import { BlackMarketEntity } from '../../../database/entities/BlackMarket.entity';
import { getConfig } from '../../../config/config';
import { MessageEmbed, TextChannel } from 'discord.js';
import { CommandsText } from '../../../logs/commands/text';

client.on('messageReactionAdd', async (react, user) => {
  if (user.bot || react.message.channel.type === 'DM') return;

  const message = react.message;
  const channel = message.guild!.channels.cache.get(
    getConfig().channels.text.blackMarket,
  ) as TextChannel;

  if (!channel) return;

  const repository = getRepository(BlackMarketEntity);
  const row = await repository.findOne({ where: { message_id: message.id } });
  if (!row || row!.decided) return;

  const member = await message.guild?.members.fetch(row?.user_id);
  if (!member) return;

  if (react.emoji.name === '❌') {
    await message.reactions.removeAll();
    await message.edit({
      embeds: [message.embeds[0].setColor('RED').setFooter('Отклонил: ' + user.username)],
    });
    await repository.delete(row);
    member.send({
      embeds: [new MessageEmbed().setColor('RED').setTitle(CommandsText.CANCEL_ADV)],
    });
    return;
  } else if (react.emoji.name === '✔') {
    await message.reactions.removeAll();
    await message.edit({
      embeds: [message.embeds[0].setColor('GREEN').setFooter('Принял: ' + user.username)],
    });
    await channel.send({ embeds: [message.embeds[0].setFooter('')] });
    const messageId = channel.lastMessage?.id;

    const newRow = await repository.create({
      message_id: messageId,
      user_id: row.user_id,
      decided: true,
    });

    await repository.delete(row);
    await repository.save(newRow);
    member.send({
      embeds: [
        new MessageEmbed()
          .setColor('RED')
          .setTitle(CommandsText.ACCEPT_ADV)
          .setFooter(`Айди объявления: ${newRow.message_id}`),
      ],
    });

    return;
  } else return;
});
