import { Message, MessageEmbed } from 'discord.js';
import { UsersEntity } from '../../../database/entities/Users.entity';
import { noun } from 'plural-ru';
import { CommandsText } from '../../../logs/commands/text';
import { buyTickets, getAvailableTickets, getLottery } from './util';

export const buyTicket = async (
  message: Message,
  user: UsersEntity,
  countStr: string,
): Promise<void> => {
  if (!countStr) return;
  const count = parseInt(countStr);
  if (isNaN(count) || count < 1) throw CommandsText.PARAMS_ERROR;

  const lottery = await getLottery();
  if (!lottery || lottery.winner) return;

  const ticketsToBuy = getAvailableTickets(lottery) >= count ? count : getAvailableTickets(lottery);

  if (user.coins >= lottery.price * ticketsToBuy) {
    await user.takeCoins(lottery.price * ticketsToBuy);
    const ticketRus = noun(
      ticketsToBuy,
      'лотерейный билет',
      'лотерейных билета',
      'лотерейных билетов',
    );

    const embed = new MessageEmbed()
      .setColor('BLUE')
      .setTitle(`Вы успешно приобрели ${ticketsToBuy} ${ticketRus}.`);

    message.author.send({ embeds: [embed] });
  } else throw CommandsText.NO_COINS;

  buyTickets(message.author.id, ticketsToBuy, lottery);
};
