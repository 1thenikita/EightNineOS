import { Message, MessageEmbed } from 'discord.js';
import { UsersEntity } from '../../database/entities/Users.entity';
import { getConfig } from '../../config/config';
import { CommandsText } from '../../logs/commands/text';
import moment from 'moment';

export const buyPng = async (message: Message, user: UsersEntity): Promise<void> => {
  const authorMember = await message.guild!.members.fetch(message.author.id);
  if (!authorMember) return;

  if (authorMember.roles.cache.has(getConfig().roles.png)) throw CommandsText.ALREADY_HAS_PNG;

  if (user.coins >= 2000) {
    const role = message.guild!.roles.cache.get(getConfig().roles.png);
    user.takeCoins(2000);
    authorMember.roles.add(role!);
    message.author.send({
      embeds: [new MessageEmbed().setColor('BLUE').setTitle(CommandsText.SUCCESS_SUBSCRIBE)],
    });
    user.png_subscribe_timeout = moment().add(7, 'day').toDate();
  } else throw CommandsText.NO_COINS;
};
