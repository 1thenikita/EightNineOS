import { Message, MessageEmbed } from 'discord.js';
import { UsersEntity } from '../../database/entities/Users.entity';
import { PricelistEntity } from '../../database/entities/Pricelist.entity';
import { CommandsText } from '../../logs/commands/text';
import { getConfig } from '../../config/config';

export const buyEquip = async (
  message: Message,
  user: UsersEntity,
  equipStr: string,
): Promise<void> => {
  if (!equipStr) return sendTemplate(message);
  const equip = parseInt(equipStr);
  if (isNaN(equip)) return sendTemplate(message);
  const wantToBuyEquip = await PricelistEntity.getPrice(equip);

  if (!wantToBuyEquip) return sendTemplate(message);
  const price = wantToBuyEquip.start_price * ++user.equipment[equip];

  if (user.equipment[equip] > getConfig().maxEquipLevel) throw CommandsText.MAX_EQUIP_LEVEL;

  if (user.coins >= price) {
    await user.takeCoins(price);
    const embedBuy = new MessageEmbed()
      .setColor('BLUE')
      .setTitle(
        `Вы успешно приобрели ${wantToBuyEquip.equipment_name} ${user.equipment[equip]} уровня`,
      );
    message.author.send({ embeds: [embedBuy] });
  } else throw CommandsText.NO_COINS;
};

const sendTemplate = (message: Message) => {
  const embed = new MessageEmbed()
    .setColor('#00AE86')
    .setDescription(
      '**Укажите номер улучшения**\n\n• [1] Шлем\n• [2] Нагрудник\n• [3] Поножи\n• [4] Сапоги\n• [5] Меч\n• [6] Лук',
    )
    .setFooter('!buyequip <номер улучшения>', message.author.avatarURL()!);

  return message.reply({ embeds: [embed] }).then((msg) => {
    setTimeout(function () {
      msg.delete();
    }, 15000);
  });
};
