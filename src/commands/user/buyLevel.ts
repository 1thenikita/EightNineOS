import { Message, MessageEmbed } from 'discord.js';
import { UsersEntity } from '../../database/entities/Users.entity';
import { CommandsText } from '../../logs/commands/text';
import { getConfig } from '../../config/config';

export const buyLevel = (message: Message, user: UsersEntity): void => {
  const levelPrice = user.getNextLevelPrice();

  if (user.level + 1 == getConfig().maxUserLevel) throw CommandsText.MAX_USER_LEVEL;

  if (levelPrice > user.coins) throw CommandsText.NO_COINS;

  user.level++;

  const embedLevelUp = new MessageEmbed()
    .setColor('BLUE')
    .setTitle('Вы успешно приобрели ' + user.level + ' уровень');
  message.author.send({ embeds: [embedLevelUp] });

  user.takeCoins(levelPrice);
};
