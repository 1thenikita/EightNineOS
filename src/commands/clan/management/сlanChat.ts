import { GuildChannel, Message } from 'discord.js';
import { UsersEntity } from '../../../database/entities/Users.entity';

export const clanChat = async (message: Message, user: UsersEntity): Promise<void> => {
  const target = message.mentions.members?.first();
  if (!user.clan.isClanLeaderOrDeputy(user) || !target) return;

  const chat = message.guild!.channels.cache.get(user.clan.chatRoom!)! as GuildChannel;
  const perm = chat!.permissionOverwrites.cache.get(target.id)?.allow.has('SEND_MESSAGES');

  await chat.permissionOverwrites.create(target.id, {
    VIEW_CHANNEL: !perm,
    SEND_MESSAGES: !perm,
    ATTACH_FILES: !perm,
  });

  await message.author.send(`${perm ? 'Закрыт' : 'Открыт'} доступ в клановый чат для ${target}`);
};
