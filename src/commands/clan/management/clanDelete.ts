import { Message, MessageEmbed } from 'discord.js';
import { UsersEntity } from '../../../database/entities/Users.entity';
import { CommandsText } from '../../../logs/commands/text';
import { sendClanLog, sendGeneral } from '../../../logs/admin/channels';

export const clanDelete = async (message: Message, user: UsersEntity): Promise<void> => {
  if (!user.clan.isClanLeaderOrDeputy(user)) throw CommandsText.USAGE_ONLY_FOR_LEADER;

  const embed = new MessageEmbed()
    .setColor('BLUE')
    .setTitle(CommandsText.SUCCESSFULLY_DELETED_CLAN);

  await sendGeneral(`${message.member} удалил клан "**${user.clan.name}**"`);
  await sendClanLog(`${message.member} удалил клан "**${user.clan.name}**"`);
  await message.author.send({ embeds: [embed] });
  await user.clan.deleteClan();
};
