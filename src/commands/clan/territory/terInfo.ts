import { Message, MessageEmbed } from 'discord.js';
import { TerritoryEntity } from '../../../database/entities/Territory.entity';
import { CommandsText } from '../../../logs/commands/text';
import { ClansEntity } from '../../../database/entities/Clans.entity';

export const terInfo = async (message: Message, user: null, terStr: string): Promise<void> => {
  const territoryID = parseInt(terStr);
  if (isNaN(territoryID)) throw CommandsText.PARAMS_ERROR;

  const territory = await TerritoryEntity.getTerritory(territoryID);
  if (!territory) return;

  const owner =
    territory.owner === null
      ? 'Государство'
      : (await ClansEntity.getClan(territory.owner as number))?.name ?? 'Государство';

  const info = `Контролирует: **${owner}**\nСтоимость территории: **${territory.price}\n**Доход: **${territory.income}** в сутки\n\n`;

  const embed = new MessageEmbed()
    .setColor('#00AE86')
    .setTitle(`Информация о территории ${territory.name}`)
    .setDescription(info);

  message.author.send({ embeds: [embed] });
};
