import { Message, MessageActionRow, MessageButton, MessageEmbed } from 'discord.js';
import { UsersEntity } from '../../database/entities/Users.entity';
import { CommandsText } from '../../logs/commands/text';
import moment from 'moment';
import { getRepository } from 'typeorm';
import { ClansEntity } from '../../database/entities/Clans.entity';
import { sendClanLog, sendGeneral } from '../../logs/admin/channels';

/**
 string - user id
 number - clan id
 */
export const pendingInvites = new Map<string, number>();

export const invite = async (message: Message, user: UsersEntity): Promise<void> => {
  const target = message.mentions.users.first();
  if (!target) throw CommandsText.PARAMS_ERROR;

  const targetUser = await UsersEntity.getOrCreateUser(target.id);
  const targetMember = await message.guild!.members.fetch(target.id);
  if (!targetMember) return;

  const isAlreadyInvited = pendingInvites.has(target.id);

  if (isAlreadyInvited) throw CommandsText.ALREADY_INVITED_TO_CLAN;

  if (!user.clan.isClanLeaderOrDeputy(user)) return;

  if (targetUser.clan) throw CommandsText.ALREADY_IN_CLAN;

  pendingInvites.set(target.id, user.clan.id);
  const role = await message.guild!.roles.fetch(user.clan.role);
  const clanID = user.clan.id;

  const clan = await getRepository(ClansEntity).findOne(clanID);
  if (!clan) return void pendingInvites.delete(target.id);

  const embedInvite = new MessageEmbed()
    .setColor(role!.hexColor)
    .setTitle(`Приглашение в клан`)
    .setDescription(`Приглашение от ${message.author.toString() ?? ''}`)
    .setFooter(`Клан ${user.clan.name}`, user.clan.emblem)
    .setTimestamp();

  const embedCancel = new MessageEmbed()
    .setColor('BLUE')
    .setTitle(CommandsText.SUCCESSFULLY_CANCEL_INVITE);

  const embedAccept = new MessageEmbed()
    .setColor('BLUE')
    .setTitle(CommandsText.SUCCESSFULLY_ACCEPT_INVITE);

  const buttonJoin = new MessageButton()
    .setCustomId('join')
    .setLabel('Вступить')
    .setStyle('SUCCESS');

  const buttonCancel = new MessageButton()
    .setCustomId('cancel')
    .setLabel('Отклонить')
    .setStyle('DANGER');

  const row = new MessageActionRow().addComponents(buttonJoin).addComponents(buttonCancel);
  const messageInvite = await target.send({ embeds: [embedInvite], components: [row] });

  const interaction = await messageInvite.awaitMessageComponent();
  if (!interaction.isButton()) return;

  const resultEmbed = new MessageEmbed()
    .setColor('RED')
    .setTitle('Ошибка')
    .setDescription(
      'Вы сможете зайти в клан только ' + moment(targetUser.clan_join_timeout).fromNow(),
    );

  if (interaction.customId == 'join') {
    const dateNow = moment();
    const canAcceptInvite =
      !targetUser.clan_join_timeout || dateNow.isAfter(targetUser.clan_join_timeout);
    if (!canAcceptInvite) {
      interaction.update({
        embeds: [resultEmbed],
        components: [row],
      });
      return;
    }

    await interaction.update({ embeds: [embedAccept], components: [] });

    await targetMember.roles.add(clan.role);

    pendingInvites.delete(target.id);

    sendGeneral(`${target} вступил(а) в клан "**${clan.name}**"`);
    sendClanLog(`${target} вступил(а) в клан "**${clan.name}**"`);

    targetUser.clan_join_timeout = null;
    targetUser.clan = clan;

    await targetUser.save();

    const clanMembersCount = await targetUser.clan.countMembers();
    if (clanMembersCount >= 7) targetUser.clan.clanChannel(message.guild!, true);
  }

  if (interaction.customId == 'cancel') {
    const invite = pendingInvites.get(target.id);
    if (invite) {
      pendingInvites.delete(target.id);
      interaction.update({ embeds: [embedCancel], components: [] });
    }
  }
};
