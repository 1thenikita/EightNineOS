import { Message } from 'discord.js';
import { UsersEntity } from '../../database/entities/Users.entity';
import { CommandsText } from '../../logs/commands/text';
import { sendClanLog, sendGeneral } from '../../logs/admin/channels';
import moment from 'moment';

export const leaveClan = async (message: Message, user: UsersEntity): Promise<void> => {
  const authorMember = message.member || (await message.guild!.members.fetch(message.author.id));
  if (!authorMember) return;

  if (user.clan.leader === message.author.id) throw CommandsText.CANNOT_LEAVE_AS_LEADER;

  if (user.clan.deputy === message.author.id) {
    // @ts-ignore
    user.clan.deputy = null;
    await user.clan.save();
  }

  await authorMember.roles.remove(user.clan.role);

  sendGeneral(`${message.author} покинул(а) клан "**${user.clan.name}**"`);
  sendClanLog(`${message.author} покинул(а) клан "**${user.clan.name}**"`);

  user.clan_join_timeout = moment().add(1, 'day').toDate();
  // @ts-ignore
  user.clan = null;
  await user.save();

  const clanMembersCount = await user.clan.countMembers();
  if (clanMembersCount < 7) user.clan.clanChannel(message.guild!, false);
};
