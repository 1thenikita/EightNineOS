/**
  string - first user id
  string - second user id
 */
import { Message, MessageEmbed } from 'discord.js';
import { UsersEntity } from '../../../database/entities/Users.entity';
import { CommandsText } from '../../../logs/commands/text';
import { getRepository } from 'typeorm';
import { LoveRoomsEntity } from '../../../database/entities/LoveRooms.entity';
import { getConfig } from '../../../config/config';
import { LoveRoomsChildrenEntity } from '../../../database/entities/LoveRoomsChildren.entity';

export const marryInvites = new Map<string, string>();

export const marry = async (message: Message, user: UsersEntity): Promise<void> => {
  const target = message.mentions.members!.first();
  if (!target) throw CommandsText.PARAMS_ERROR;

  if (target.id == message.author.id) return;

  const isAlreadyInvited = marryInvites.has(target.id);
  if (isAlreadyInvited) throw CommandsText.ALREADY_REQUESTED_MARRY;

  const ifAlreadyInvite = marryInvites.has(message.author.id);
  if (ifAlreadyInvite) throw CommandsText.ALREADY_REQUEST_MARRY;

  const roomRepository = getRepository(LoveRoomsEntity);
  const childRepository = getRepository(LoveRoomsChildrenEntity);
  const isInFamilyFirst = await roomRepository
    .createQueryBuilder('family')
    .where('family.firstUser_id = :id OR family.secondUser_id = :id', {
      id: message.author.id,
    })
    .getOne();

  const isInFamilySecond = await roomRepository
    .createQueryBuilder('family')
    .where('family.firstUser_id = :id OR family.secondUser_id = :id', {
      id: target.id,
    })
    .getOne();

  if (isInFamilyFirst || isInFamilySecond) throw CommandsText.ALREADY_FAMILY;

  await UsersEntity.getOrCreateUser(target.id);

  if (user.coins < getConfig().loveRoomExtendPrice) throw CommandsText.NO_COINS;

  marryInvites.set(target.id, message.author.id);
  const child = await childRepository.find({ where: [{ user_id: message.author.id }] });
  if (child.length != 0) {
    message.author.send({
      embeds: [new MessageEmbed().setColor(16426470).setDescription(CommandsText.LEFT_FAMILY)],
    });
    for (const children of child) {
      await childRepository.delete(children);
    }
  }

  message.author.send({
    embeds: [
      new MessageEmbed()
        .setColor(16426470)
        .setDescription(
          `Вы предложили **руку и сердце** ${target}. Ждите ответа и верьте, что ваши чувства окажутся взаимными!`,
        ),
    ],
  });

  target.send({
    embeds: [
      new MessageEmbed()
        .setColor(16426470)
        .setDescription(
          `Пользователь ${message.author} сделал вам **предложение руки и сердца**. Не томите его ожиданием, ведь это худшая пытка для влюбленного человека...`,
        )
        .setFooter(CommandsText.MARRY_COMMANDS),
    ],
  });
};
