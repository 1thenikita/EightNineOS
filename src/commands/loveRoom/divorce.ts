import { getRepository } from 'typeorm';
import { LoveRoomsEntity } from '../../database/entities/LoveRooms.entity';
import { Message, MessageEmbed, TextChannel } from 'discord.js';
import { UsersEntity } from '../../database/entities/Users.entity';
import { sendLoveLog } from '../../logs/admin/channels';
import { client } from '../../client';
import { getConfig } from '../../config/config';
import { LoveRoomsChildrenEntity } from '../../database/entities/LoveRoomsChildren.entity';
import { CommandsText } from '../../logs/commands/text';

export const divorce = async (message: Message, user: UsersEntity): Promise<void> => {
  const channel = client.channels.cache.get(getConfig().channels.text.general) as TextChannel;
  const roomRepository = getRepository(LoveRoomsEntity);

  const family = await roomRepository.findOne({
    where: [{ firstUser_id: message.author.id }, { secondUser_id: message.author.id }],
  });

  if (!family) throw CommandsText.NO_FAMILY;

  const secondMemberId =
    family.firstUser_id == message.author.id ? family.secondUser_id : family.firstUser_id;
  const secondMember = await message.guild!.members.fetch(secondMemberId);
  const secondUser = await UsersEntity.getOrCreateUser(secondMemberId);

  const coinsToEvery = (family.balance - getConfig().commission * family.balance) / 2;

  await user.giveCoins(coinsToEvery);
  await secondUser?.giveCoins(coinsToEvery);

  const sendTextLog = `**Развод:** ${message.author} и ${secondMember} получили по **${coinsToEvery}** коинов.`;

  channel.send({
    embeds: [
      new MessageEmbed()
        .setColor(16426470)
        .setTitle(`**💔 РАЗВОД 💔**`)
        .setDescription(
          `Сегодня ${message.author} и ${secondMember} расторгнули **брак...** \n**Пожелаем разведённым счастья в новых начинаниях, а ребёнку удачи в детдоме, если, конечно, он был 🍷**`,
        )
        .setImage(
          'https://cdn.discordapp.com/attachments/664396125674930177/886686408222318602/kstr-kochstrasse.gif',
        ),
    ],
  });

  const childrenRepository = getRepository(LoveRoomsChildrenEntity);
  const children = await childrenRepository.find({
    where: [{ room_id: family.room_id }],
  });

  for (const child of children) {
    const childMember = await message.guild?.members.fetch(child.user_id);
    if (!childMember) return;
    childMember.roles.add(getConfig().roles.orphanage);
    await childrenRepository.delete(child);
  }

  await sendLoveLog(sendTextLog);

  await family.sendToAllMembers(
    new MessageEmbed()
      .setColor('RED')
      .setTitle(`**РАЗВОД**`)
      .setDescription(
        `${message.member} стал инициатором развода. Семья распалась, родители поделили между собой семейный бюджет.`,
      ),
  );

  await roomRepository.delete(family);
  await family.deleteRoom();
};
