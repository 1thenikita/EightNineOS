import { client } from './';
import { UsersEntity } from '../database/entities/Users.entity';

client.on('guildMemberRemove', async (member) => {
  const user = await UsersEntity.getOrCreateUser(member.id);
  // @ts-ignore
  user.clan = null;

  user.save();
});
