import { VoiceState } from 'discord.js';
import { client } from '../client';
import { getConfig } from '../config/config';

client.on('voiceStateUpdate', async (oldVoice: VoiceState, newVoice: VoiceState) => {
  if (newVoice.member!.roles.cache.has(getConfig().roles.mute))
    return void newVoice.member!.voice.disconnect().catch(() => {});

  if (newVoice.channel?.id === getConfig().channels.voice.createRoom) {
    newVoice.guild.channels
      .create(`Приват ${newVoice.member!.displayName}`, {
        type: 'GUILD_VOICE',
        parent: getConfig().channels.voice.createRoomCategory,
        permissionOverwrites: [
          {
            id: getConfig().roles.admin,
            allow: ['MOVE_MEMBERS'],
          },
          {
            id: getConfig().roles.mute,
            deny: ['CONNECT', 'SPEAK', 'VIEW_CHANNEL'],
          },
          {
            id: newVoice.member!.id,
            allow: ['MANAGE_CHANNELS'],
          },
        ],
      })
      .then((a) => {
        newVoice.member!.voice.setChannel(a).catch(() => {});
      });
  }

  if (
    oldVoice.channel?.parentId === getConfig().channels.voice.createRoomCategory &&
    oldVoice.channel!.id !== getConfig().channels.voice.createRoom &&
    oldVoice.channel!.members.size === 0
  ) {
    oldVoice.channel!.delete().catch(() => {});
  }
});
