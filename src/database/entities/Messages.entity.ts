import { Column, Entity, getRepository, PrimaryColumn, Repository } from 'typeorm';
import { DeleteResult } from 'typeorm';

@Entity('messages')
export class MessagesEntity {
  @PrimaryColumn({
    type: 'varchar',
    length: 180,
  })
  title: string;

  @Column('text')
  message: string;

  static get repository(): Repository<MessagesEntity> {
    return getRepository(MessagesEntity);
  }

  delete(): Promise<DeleteResult> {
    return getRepository(MessagesEntity).delete(this.title);
  }

  save(): Promise<MessagesEntity> {
    return getRepository(MessagesEntity).save(this);
  }
}
