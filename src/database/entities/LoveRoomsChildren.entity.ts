import { Column, Entity, getRepository, JoinColumn, ManyToOne, PrimaryColumn } from 'typeorm';
import { LoveRoomsEntity } from './LoveRooms.entity';

@Entity('love_rooms_children')
export class LoveRoomsChildrenEntity {
  @PrimaryColumn('varchar', { length: 18 })
  user_id: string;

  @JoinColumn({ name: 'room_id' })
  @ManyToOne(() => LoveRoomsEntity, (room) => room.children)
  room_id: LoveRoomsEntity;

  public save(): Promise<LoveRoomsChildrenEntity> {
    return getRepository(LoveRoomsChildrenEntity).save(this);
  }
}
