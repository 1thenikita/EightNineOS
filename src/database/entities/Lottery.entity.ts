import { Entity, PrimaryGeneratedColumn, Column, OneToMany, getRepository, IsNull } from 'typeorm';
import { UserTicketsEntity } from './UserTickets.entity';

@Entity('lottery')
export class LotteryEntity {
  @PrimaryGeneratedColumn()
  lottery_id!: number;

  @Column('int')
  price!: number;

  @Column('int')
  maxTickets!: number;

  @Column()
  message_id!: string;

  @Column({ type: 'varchar', nullable: true })
  winner!: string | null;

  @OneToMany(() => UserTicketsEntity, (userTicket) => userTicket.lottery, {
    eager: true,
  })
  userTickets: Array<UserTicketsEntity>;

  static getActiveLottery(): Promise<LotteryEntity | undefined> {
    return getRepository(LotteryEntity).findOne({ where: { winner: IsNull() } });
  }

  static async createLottery(
    price: number,
    maxTickets: number,
    message_id: string,
  ): Promise<LotteryEntity> {
    const entity = getRepository(LotteryEntity).create({
      price,
      maxTickets,
      message_id,
      userTickets: [],
    });

    return getRepository(LotteryEntity).save(entity);
  }

  async setWinner(winner: string): Promise<void> {
    this.winner = winner;
    await getRepository(LotteryEntity).save(this);
  }

  async buyTickets(user_id: string, count: number): Promise<void> {
    const userTickets =
      this.userTickets.find((t) => t.user_id === user_id) ||
      (await UserTicketsEntity.addUserToLottery(user_id, this));

    userTickets.count += count;
    this.userTickets = [...this.userTickets.filter((t) => t.user_id !== user_id), userTickets];

    await getRepository(UserTicketsEntity).save(userTickets);
  }

  async delete(): Promise<void> {
    const repository = getRepository(UserTicketsEntity);

    await repository.delete(this.userTickets.map((user) => user.user_ticket_id));

    await getRepository(LotteryEntity).delete(this.lottery_id);
  }
}
