import { Column, Entity, getRepository, OneToMany, PrimaryColumn } from 'typeorm';
import { LoveRoomsChildrenEntity } from './LoveRoomsChildren.entity';
import moment from 'moment';
import { client } from '../../client';
import { getConfig } from '../../config/config';
import { MessageEmbed, User } from 'discord.js';
import { CommandsText } from '../../logs/commands/text';

@Entity('love_rooms')
export class LoveRoomsEntity {
  @PrimaryColumn('varchar', { length: 18 })
  room_id: string;

  @Column('varchar', { length: 18 })
  firstUser_id: string;
  @Column('varchar', { length: 18 })
  secondUser_id: string;

  @OneToMany(() => LoveRoomsChildrenEntity, (user) => user.room_id)
  children: LoveRoomsChildrenEntity[];

  @Column('date')
  childCD: Date;

  @Column({ type: 'date', default: moment().format('YYYY-MM-DD') })
  last_payment_date: Date;

  @Column('int', { default: 0 })
  balance: number;

  public save(): Promise<LoveRoomsEntity> {
    return getRepository(LoveRoomsEntity).save(this);
  }

  async takeCoins() {
    this.balance -= getConfig().loveRoomExtendPrice;
    this.last_payment_date = moment().toDate();

    await this.save();
  }

  sendToFamilyParents(text: string) {
    const guild = client.guilds.cache.get(getConfig().guild)!;

    const firstMember = guild.members.cache.get(this.firstUser_id);
    const secondMember = guild.members.cache.get(this.secondUser_id);

    if (firstMember) {
      firstMember.send(text);
    }

    if (secondMember) {
      secondMember.send(text);
    }
  }

  async sendToAllMembers(text: MessageEmbed) {
    const guild = client.guilds.cache.get(getConfig().guild)!;

    const firstMember = guild.members.cache.get(this.firstUser_id);
    const secondMember = guild.members.cache.get(this.secondUser_id);

    if (firstMember) {
      firstMember.send({ embeds: [text] });
    }

    if (secondMember) {
      secondMember.send({ embeds: [text] });
    }

    if (this.children) {
      for (const user of this.children) {
        const member = await guild.members.fetch(user.user_id);

        if (member) {
          member.send({ embeds: [text] });
        }
      }
    }
  }

  async deleteRoom() {
    const guild = client.guilds.cache.get(getConfig().guild);
    if (!guild) return;

    const room = guild.channels.cache.get(this.room_id)!;

    await room.delete();

    const roomRepository = getRepository(LoveRoomsEntity);
    await roomRepository.delete(this.room_id);
  }

  async checkFullFamily(author: User, roomId: string) {
    const loveRoomRepository = getRepository(LoveRoomsEntity);

    const family = await loveRoomRepository
      .createQueryBuilder('family')
      .leftJoinAndSelect('family.children', 'children')
      .where('family.room_id = :id', {
        id: roomId,
      })
      .getOne();
    if (!family) return;
    return family;
  }
}
