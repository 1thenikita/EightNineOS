import { DeleteResult, Column, Entity, getRepository, Repository, PrimaryColumn } from 'typeorm';

export const enum SettingsTypes {
  LAST_PASTA = 0,
}

@Entity('settings')
export class SettingsEntity {
  @PrimaryColumn()
  type!: SettingsTypes;

  @Column()
  value!: string;

  static get repository(): Repository<SettingsEntity> {
    return getRepository(SettingsEntity);
  }

  delete(): Promise<DeleteResult> {
    return getRepository(SettingsEntity).delete({
      type: this.type,
    });
  }

  save(): Promise<SettingsEntity> {
    return getRepository(SettingsEntity).save(this);
  }
}
