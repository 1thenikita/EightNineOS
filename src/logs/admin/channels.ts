import { client } from '../../client';
import { Message, MessageEmbed, TextChannel } from 'discord.js';
import { getConfig } from '../../config/config';

export const sendCoinsLog = (text: string): void => {
  (client.channels.cache.get(getConfig().channels.text.coinsLog) as TextChannel).send(text);
};

export const sendGeneral = (obj: MessageEmbed | string): Promise<Message> => {
  return (client.channels.cache.get(getConfig().channels.text.general) as TextChannel).send(
    obj instanceof MessageEmbed ? { embeds: [obj] } : obj,
  );
};

export const sendWarnLog = (text: string): void => {
  (client.channels.cache.get(getConfig().channels.text.warnLog) as TextChannel).send(text);
};

export const sendMuteLog = (text: string): void => {
  (client.channels.cache.get(getConfig().channels.text.muteLog) as TextChannel).send(text);
};

export const sendClanLog = (text: string): void => {
  (client.channels.cache.get(getConfig().channels.text.clanLog) as TextChannel).send(text);
};

export const sendAPILog = (text: string): void => {
  (client.channels.cache.get(getConfig().channels.text.APILog) as TextChannel).send(text);
};

export const sendEventsLog = (text: string): Promise<Message> => {
  return (client.channels.cache.get(getConfig().channels.text.eventsLog) as TextChannel).send(text);
};

export const sendRoleLog = (text: string): void => {
  const embed = new MessageEmbed().setDescription(text);
  (client.channels.cache.get(getConfig().channels.text.rolesLog) as TextChannel)?.send({
    embeds: [embed],
  });
};

export const sendCustomRolesLog = (text: string): Promise<Message> => {
  return (client.channels.cache.get(getConfig().channels.text.customRolesLogs) as TextChannel).send(
    text,
  );
};

export const sendModdc = (embed: MessageEmbed): Promise<Message> => {
  return (client.channels.cache.get(getConfig().channels.text.moddc) as TextChannel).send({
    embeds: [embed],
  });
};

export const sendLoveLog = (text: string): Promise<Message> => {
  return (client.channels.cache.get(getConfig().channels.text.loveLog) as TextChannel).send(text);
};

export const sendVoiceLog = (embed: MessageEmbed): Promise<Message> => {
  return (client.channels.cache.get(getConfig().channels.text.voiceLog) as TextChannel).send({
    embeds: [embed],
  });
};
