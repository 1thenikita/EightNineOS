const reqUsers = [
  { name: 'hojitaler', token: process.env.TOKEN_API_hojitaler },
  { name: 'Musk', token: process.env.TOKEN_API_Musk },
];

export { reqUsers };
